<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/* Front-end routes */
Route::get('/teams', 'Front\TeamController@index')->name('show.teams');
Route::get('/players', 'Front\PlayerController@index')->name('show.players');

/* Dashboard routes */
Route::get('/dashboard/teams', 'Dashboard\TeamController@index')->name('dashboard.teams');
Route::get('/dashboard/setting', 'Dashboard\SettingController@index')->name('dashboard.setting');

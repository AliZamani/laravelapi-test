<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Country;

/**
 * 
 * @author 3ehrang
 * 
 * Api country controller
 *
 */
class CountryController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
		// secure api
        $this->middleware('auth:api', ['except' => array('index')]);
	}
	
	/**
	 * Get all teams
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\App\Country[]
	 */
	public function index()
	{
		return Country::all();
	}
	
}

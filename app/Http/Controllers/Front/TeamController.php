<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeamController extends Controller
{
	/**
	 * Get all teams
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\App\Team[]
	 */
	public function index()
	{
		return view('teams');
	}
}

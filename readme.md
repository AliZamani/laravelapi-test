# Laravel REST-full API Testing Project

Create a REST API using the Laravel PHP framework to model sports teams and their players.

This Project uses only API with Vue.js component for creating front and back end pages.

- Migration schema for creating the tables.
- Using seed script to fill the tables with some data.
- Creating API endpoint to add/update/delete/list teams/players tables.
- Authentication method Using Laravel Passport package, so only trusted entities may call the APIs.
- Using PHPUnit Test for teams/players action.
- It's a testing project so request validation is not added.

### Schema for countries table:
- id
- name
- created_at
- updated_at

### Schema for teams table:
- id
- name
- country_id
- description
- created_at
- updated_at
 
### Schema for players table:
- id
- team_id
- first_name
- last_name
- created_at
- updated_at

## Installation

- Copy or rename .env.example to .env
	
- Create database and put database information in the .env file

- Run Composer Update

	composer update

- Genereate new app key:

	php artisan key:generate
	
- Create tables and seeding data:

	php artisan migrate --seed

- Create the encryption keys needed to generate secure access tokens.

    php artisan passport:install


## Documention

### 1- Front End
You can see teams and players page as a guest user

### 2- Back End
Create a new user with register link to access managing team and setting page

### 3- API
This project uses Laravel Passport package for securing API end point by token.

To retrieve a token, make a request to the oauth/token endpoint. full doc here:
	[API Authentication (Passport)](https://laravel.com/docs/5.5/passport#creating-a-personal-access-client)

> For testing purpose simply register new user in back end then login and go to API setting page and create new personal token
do not forget to include this token to any API requests.

**API usage: Team**

1- create new team:

    POST /api/teams
    Form data:
        name:{team name}
        description:{any description about team}
        country_id:{country id integer between 1-20}
    Headers:
        Authorization: Bearer {token}

2- Update a team:

    PUT /api/teams/{id}
    Form data:
        name:{team name}
        description:{any description about team}
        country_id:{country id integer between 1-20}
    Headers:
        Authorization: Bearer {token}
3- Delete a team:

    DELETE /api/teams/{id}
    Headers:
        Authorization: Bearer {token}
4- Get List of the teams:
*No token authentication required*

    GET /api/teams

5- Get a team by id:

    GET /api/teams/{id}
    Headers:
        Authorization: Bearer {token} 
    
**API usage: Players** *Just like team API*

1- create new team:

    POST /api/teams
    Form data:
        team_id:{id}
        first_name:Behrang
        last_name:{Nourmohamadi}
    Headers:
        Authorization: Bearer {token}
2- *Other requests are just like team section*

### 4- Testing
- Run PHPUnit Test by this command

	./vendor/bin/phpunit

### Laravel 5.5

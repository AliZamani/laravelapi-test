<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

// for team
$factory->define(App\Player::class, function (Faker $faker) {
	return [
			'team_id' => App\Team::inRandomOrder()->first()->id,
			'first_name' => $faker->firstName,
			'last_name' => $faker->unique()->lastName,
	];
});

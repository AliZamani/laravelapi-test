<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * All testing unit for team api contains create, update, delete, list actions
 * 
 * @author 3ehrang
 *
 */
class TeamTest extends TestCase
{
	
	public function testsTeamIsCreatedCorrectly()
	{
		$faker = \Faker\Factory::create();
		$team = [
				'name' => 'Test Unit ' . $faker->unique()->name,
				'description' => 'Test Unit ' . $faker->text,
				'country_id' => 2
		];
		$this->json('POST', '/api/teams', $team)
			->assertStatus(201)
			->assertJson($team)
		;
	}
	
	public function testsTeamIsUpdatedCorrectly()
	{
		$team = factory(\App\Team::class)->create([
					'name' => 'Happy Happy Team One',
					'description' => 'Happy Team lives in Happy Nation',
					'country_id' => 2
		]);
		
		$payload = [
				'name' => 'Still Happy Team',
				'description' => 'Happy Team lives in Happy Nation',
				'country_id' => 2
		];
		$response = $this->json('PUT', '/api/teams/' . $team->id, $payload)
			->assertStatus(200)
			->assertJson([
					'name' => 'Still Happy Team',
					'description' => 'Happy Team lives in Happy Nation',
					'country_id' => 2
			])
		;		
	}
	
	public function testsTeamIsDeletedCorrectly()
	{
		$team = factory(\App\Team::class)->create([
				'name' => 'Happy Happy Team One',
				'description' => 'Happy Team lives in Happy Nation',
				'country_id' => 2
		]);
		
		$this->json('DELETE', '/api/teams/' . $team->id)
			->assertStatus(204);
	}
	
	public function testTeamsAreListedCorrectly()
	{
		$country = factory(\App\Country::class)->create([
				'name' => 'Happy Nation'
		]);
		
		factory(\App\Team::class)->create([
				'name' => 'Happy Team',
				'description' => 'Happy Team lives in Happy Nation',
				'country_id' => $country->id
		]);
		
		$response = $this->json('GET', '/api/teams');
		$response
			->assertStatus(200)
			->assertJsonFragment([
					'name' => 'Happy Team',
			])
			->assertJsonStructure([
				'*' => ['id', 'country_id', 'name', 'description', 'created_at', 'updated_at']
			])
		;
	}

	public function testTeamsFrontPageTest()
    {
		$response = $this->get('/teams');
        $response->assertStatus(200);
    }
}

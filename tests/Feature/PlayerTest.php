<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * All testing unit for player api contains create, update, delete, list actions
 *
 * @author 3ehrang
 *
 */
class PlayerTest extends TestCase
{
    public function testsPlayerIsCreatedCorrectly()
	{
		$faker = \Faker\Factory::create();
		$player = [
				'team_id' => 1,
				'first_name' => 'Behrang',
				'last_name' => 'Nour'
		];
		$this->json('POST', '/api/players', $player)
			->assertStatus(201)
			->assertJson($player)
		;
	}
	
	public function testsPlayerIsUpdatedCorrectly()
	{
		$player = factory(\App\Player::class)->create([
				'team_id' => 1,
				'first_name' => 'Behrang',
				'last_name' => 'Nour'
		]);
		
		$payload = [
				'team_id' => 1,
				'first_name' => 'Behrang Edited',
				'last_name' => 'Nour Edited'
		];
		$response = $this->json('PUT', '/api/players/' . $player->id, $payload)
		->assertStatus(200)
		->assertJson([
				'team_id' => 1,
				'first_name' => 'Behrang Edited',
				'last_name' => 'Nour Edited'
		])
		;
	}
	
	public function testsPlayerIsDeletedCorrectly()
	{
		$player = factory(\App\Player::class)->create([
				'team_id' => 1,
				'first_name' => 'Behrang',
				'last_name' => 'No'
		]);
		
		$this->json('DELETE', '/api/players/' . $player->id)
		->assertStatus(204);
	}
	
	public function testPlayersAreListedCorrectly()
	{
		$country = factory(\App\Country::class)->create([
				'name' => 'Happy Nation'
		]);
		
		$team = factory(\App\Team::class)->create([
				'name' => 'Happy Team',
				'description' => 'Happy Team lives in Happy Nation',
				'country_id' => $country->id
		]);
		
		factory(\App\Player::class)->create([
				'team_id' => $team->id,
				'first_name' => 'Behrang',
				'last_name' => 'Nour'
		]);
		
		$response = $this->json('GET', '/api/players');
		$response
		->assertStatus(200)
		->assertJsonFragment([
				'first_name' => 'Behrang',
		])
		->assertJsonStructure([
				'*' => ['id', 'team_id', 'first_name', 'last_name', 'created_at', 'updated_at']
		])
		;
	}

	public function testPlayersFrontPageTest()
    {
		$response = $this->get('/players');
        $response->assertStatus(200);
    }
}
